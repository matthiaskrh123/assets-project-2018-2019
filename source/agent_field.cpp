/********************************************************************
* MATTHIAS' CODE FOR READING ARDUINO VALUES THROUGH SERIAL
********************************************************************/

#include "support/configCosmos.h"
#include "agent/agentclass.h"
#include "device/serial/serialclass.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <dirent.h>
#include <string>
//#define NUM_DATA 2
using namespace std;
Agent *agent;
ofstream file;


void clearOutgoing(){
    // These are data types defined in the "dirent" header
    DIR *theFolder = opendir("C:/cosmos/nodes/cubesat1/outgoing/assets");
    struct dirent *next_file;
    char filepath[256];

    while ( (next_file = readdir(theFolder)) != NULL )
    {
        // build the path for each file in the folder
        sprintf(filepath, "%s/%s", "C:/cosmos/nodes/cubesat1/outgoing/assets", next_file->d_name);
        remove(filepath);
    }
    closedir(theFolder);
}



// use: agent_arduino <agent_name> <device path> <sensor number(ex: 000,001,...)>
int main(int argc, char** argv)
{
    cout << "Agent Arduino" << endl;
    Agent *agent;
    string nodename = "cubesat1";
    string agentname = "arduino"; //name of the agent that the request is directed to
    string soh;

    // if there is no arguments use default serial port
    string serial_port = "COM3"; // or \\\\.\\COM3
    size_t serial_baud = 9600;
    string sensor_num = "000";

    if(argc >1)
        agentname = argv[1];
    if (argc >2) {
        serial_port = argv[2];
    }
    if(argc >3){
        sensor_num = argv[3];
    }

    agent = new Agent(nodename, agentname);

    //Set SOH String
    // include all namespace names used - the names that are printed from the arduino serial port

     soh= "{\"node_loc_utc\","
                "\"node_loc_pos_eci\","
                "\"device_tsen_temp_"+sensor_num+"\"}" ;

    agent->set_sohstring(soh);

    ElapsedTime et;
    et.start();

    // set up reading from serial port
    Serial serial_arduino = Serial(serial_port,serial_baud);
    serial_arduino.set_timeout(0, 3.);

    cout << "serial port: " << serial_port << " at " << serial_baud << endl;
    if (serial_arduino.get_error() < 0) {
        // there was error opening the serial port, close the program
        cout << "error opening serial port: " << serial_arduino.get_error()  << endl;
        return 0;
    }

    // Data writing parameters
    int max_lines = 20;
    int current_line = 0;
    int file_counter = 0;

    string databuffer = "";

    // clear outgoing before test?
    clearOutgoing();

    // Start executing the agent
     while(agent->running())
    {

        int32_t status;
        string jsonstring;

        // read serial port from the arduino
        // reads the first line and saves it in jsonstring
        status = serial_arduino.get_string(jsonstring, '\n');
        if(status > 0){

            // parse jsonstring and save data to the agent
            status = json_parse(jsonstring, agent->cinfo);

            // sanity check
            cout<<jsonstring<<endl;
        }

        // writing data and date to file in outgoing

        if(current_line < max_lines){
            databuffer += jsonstring;
            current_line += 1;
        }

        else {
            string outpath = "C:/cosmos/nodes/cubesat1/outgoing/assets/data" + std::to_string(file_counter) + ".txt";
            ofstream outfile (outpath);
            outfile << jsonstring << endl;
            outfile.close();

            file_counter += 1;
        }

        //sleep for 1 sec
        COSMOS_SLEEP(1);
    }

    return 0;
}




